import os
from typing import Union
from game import Game


class Controller:
    def __init__(self, game:Game):
        self.game: Game = game
        self.round: bool = True
        self.game_end: bool = False
        
    def __clear_screen(self)->None:
        os.system('cls' if os.name == 'nt' else 'clear')

    def start(self) -> None:
        ''' Start the gameplay

        '''
        if self.round and not self.game_end:
            
            # Start Players turn
            self.__turn_start()

            # Take a card from the discard pile (open) or the card deck (closed)
            self.__take_card()
           
            # Ready to go on the table? Fill the slots!
            
            # Step 3: Get on the table and allow cards to be added to the table 

            # Step 4: Return a card to to the discard pile
            self.__return_card()
            
            print('\n')
            for index, card in enumerate(self.game.active_player.hand):
                print(f'{index} -> {card}')
            card_index: int = self.__get_player_input("Card to put back on the discard pile (0 - 10)", list(range(11)), True)

            if self.game.active_player.hand[card_index][0] == 'aussetzen':
                print('\nPlayers in the game:')
                for index, player in enumerate(self.game.players):
                    if player is not self.game.active_player:
                        print(f'{index} -> {player.name}')
                player_to_give_aussetzen: int = self.__get_player_input("Player to give the aussetzen card", [x for x,y in enumerate(self.game.players) if y is not self.game.active_player], True)
                self.game.players[player_to_give_aussetzen].aussetzen = True
            else:
                self.game.discard_pile.add_cards([self.game.active_player.hand[card_index]])
            
            self.game.active_player.remove_card_from_hand(card_index)
            
            # switch to the next player
            self.game.active_player = self.game.next_player()
            self.start()
            


    def __print_header(self) -> None:
        print('''   ___ _                    _  ___  
  / _ \ |__   __ _ ___  ___/ |/ _ \ 
 / /_)/ '_ \ / _` / __|/ _ \ | | | |
/ ___/| | | | (_| \__ \  __/ | |_| |
\/    |_| |_|\__,_|___/\___|_|\___/ 
                                     \nPress "ctrl+c" to quit the game\n\n''')
        

    def __print_player_info(self) -> str:
        print(f'Player: {self.game.active_player.name}\nPhase: {self.game.active_player.phase} ({self.game.phases[self.game.active_player.phase]["name"]})\n')
        for card in self.game.active_player.hand:
            print(f'{card}')

    def __get_player_input(self, description: str ='', conditions: list = ['y','n'], integer_value: bool = False) -> Union[str, int]:
        print(f"\n{description}:")
        while True:
            player_input = input("> ")
            if integer_value:
                player_input = int(player_input)
                            
            if player_input in conditions:
                return player_input
            else:
                print(f"Not a correct input. {description}:")



    def __turn_start(self) -> None:
        self.__clear_screen()
        self.__print_header()

        # check for aussetzen card
        if self.game.active_player.aussetzen:
            print(f'\nSorry {self.game.active_player.name}, you have to skip this round because of a Aussetzen card\n')
            self.game.active_player.aussetzen = False
            if self.__get_player_input('Type "y" to continue'):
                self.game.active_player = self.game.next_player()
                self.start()

        if self.__get_player_input(f'Next player is {self.game.active_player.name}, {self.game.active_player.name} type "y" to continue.'):
            self.__clear_screen()
            self.__print_header()
            self.__print_player_info()

    def __take_card(self) -> bool:
        has_cards, discard_pile = self.game.discard_pile.show()
        print(f'\n{discard_pile}\n')

        if has_cards:
            take_discard_pile: str = self.__get_player_input("Take the card from discard pile? (y/n)")
            if take_discard_pile == "y":
                self.game.active_player.add_card_to_hand(self.game.discard_pile.pick_card())
                return True
        self.game.active_player.add_card_to_hand(self.game.card_deck.pick_card())
        return True
    
    def __return_card(self) -> bool:
        return True