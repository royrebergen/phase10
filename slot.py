import sys

class Slot:
    def __init__(self, size: int, type: str = 'count'):
        self.type: str = type
        self.size: int = size
        self.cards: list = []

    def __str__(self) -> str:
        return f'Slot({self.type}, {self.size}, {self.cards})'
    
    def add_card(self, card:tuple) -> bool:
        """Add a card from the players hand to the slot

        Keyword arguments:
        card -- a card from the players hand
        
        Return value:
        check_slots() -- Boolean that tells if the slot is completed
        """
        if len(self.cards) == self.size:
            sys.exit("No more cards allowed in this slot!")
        if card[0] == 'aussetzen':
            sys.exit("Aussetzen card is not allowed in the phase!")
        self.cards.append(card)
        return self.__check_slot()

    def __check_slot(self) -> bool:
        """Check if the slot matches the slots conditions
        
        Uses the self.type variable to determine what to check (count, sequence, or color)

        Return value:
        check_slots() -- Boolean that tells if the slot is completed
        """
        if len(self.cards) != self.size:
            return False
        types = {'color':0,'count':1}
        if self.type in types:
            return self.__check_count(types)
        if self.type == 'sequence':
            return self.__check_sequence()
        return False

    def __check_count(self, types:dict) -> bool:
        '''Check if there is a set of cards
        
        A card tuple exists of a color and a number eg (green, 10) or (joker, joker)
        For a set of the same color cards we use the 0 index of the tuple,
        and for a set of the same numbers, we use the 1 index of the tuple.
        Jokers are always correct.
        
        Keyword arguments:
        types -- color or count; to determine if we need the 0 or 1 index
        
        Return value:
        Boolean that tells if there is a valid count
        '''
        val = set(list(zip(*self.cards))[types[self.type]])
        if len(val) == 1:
            return True
        if len(val) == 2:
            if 'joker' in val:
                return True
        return False

    def __check_sequence(self) -> bool:
        '''Check if there is a sequence of cards
        
        A card tuple exists of a color and a number eg (green, 10) or (joker, joker)
        For a sequence of cards we use the 1 index of the tuple.
        Jokers are always correct.
        
        Return value:
        Boolean that tells if there is a valid sequence
        ''' 
        hand = list(zip(*self.cards))[1]
        # check for a all jokers sequence
        uniques = set(hand) 
        if 'joker' in uniques and len(uniques) == 1:
            return True
        
        # search for the first int type
        start_index = [ind for ind, val in enumerate(hand) if isinstance(val, int)][0]
        
        # start value of the int can't be lower than its index postion in the list
        if hand[start_index] > start_index:
            # check if there is a complete sequence
            for index, value in enumerate(hand):
                if (value != hand[start_index] + (index - start_index)) and (value != "joker"):
                    return False
            return True
        return False

    def remove_card(self) -> None:
        pass

    def slot_to_table(self) -> None:
        pass