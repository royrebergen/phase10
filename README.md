# Phase10

Play the game of Phase10 in the terminal.

## Name
Phase10

## Description
...

## Installation
Python modules used:
 * os
 * typing
 * random
 * sys

## Roadmap

- [x] Sequence check of slot
- [x] Calculate penalty points by the hand
- [ ] Create gameplay controller
    - [ ] Create round
    - [ ] Scoreboard
    - [ ] Table play
    - [x] Aussetzen card logic
        - [ ] Calculate "player rounds" aussetzen only allowed when player didn't had one in the previous round
    - [ ] When card deck is empty; take all but one from discard pile and shuffle
- [ ] Create conf file
    - [ ] Language options
    - [ ] Gameplay options (variations in phases and gameplay)
- [ ] Visual represenation of classes to add in the readme

## License
GNU General Public License v3.0

## Project status
In development