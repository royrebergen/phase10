class Deck():
    
    def show_deck(self) -> None:
        """Show the cards in the deck"""
        print(self.deck)
        print(len(self.deck))

    def pick_card(self) -> list:
        """Pick the top card from the deck
        
        Return value:
        card -- List of the selected card
        """
        if self.deck:
            return [self.deck.pop()]

    def add_cards(self, cards:list) -> None:
        """Add one or more cards to the deck"""
        self.deck.extend(cards)