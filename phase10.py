from game import Game
from controller import Controller

phase10 = Game(players=['Eva', 'Roy'])
gameController = Controller(phase10)
gameController.start()