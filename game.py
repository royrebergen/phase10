import random
import sys

from player import Player
from card_deck import CardDeck
from discard_pile import DiscardPile

class Game:
    def __init__(self, players: list = ['Player 1', 'Player 2']):

        self.players: list = []
    
        self.phases = { 1:{
                            "name":"Zwei Drillinge",
                            "slots":[('count', 3), ('count', 3)]
                        },
                        2:{
                            "name":"Drilling und Viererfolge",
                            "slots":[('count', 3), ('sequence', 4)]
                        },
                        3:{
                            "name":"Vierling und Viererfolge",
                            "slots":[('count', 4), ('sequence', 4)]
                        },
                        4:{
                            "name":"Siebenerfolge",
                            "slots":[('sequence', 7)]
                        },
                        5:{
                            "name":"Achterfolge",
                            "slots":[('sequence', 8)]
                        },
                        6:{
                            "name":"Neunerfolge",
                            "slots":[('sequence', 9)]
                        },
                        7:{
                            "name":"Zwei Vierlinge",
                            "slots":[('count', 4), ('count', 4)]
                        },
                        8:{
                            "name":"Sieben Karten einer Farbe",
                            "slots":[('color', 7)]
                        },
                        9:{
                            "name":"Fünfling und Zwilling",
                            "slots":[('count', 5), ('count', 2)]
                        },
                        10:{
                            "name":"Fünfling und Drilling",
                            "slots":[('count', 5), ('count', 3)]
                        }
                    }
        
        # create 2 decks (a card deck and a discard pile) 
        self.card_deck: CardDeck = CardDeck(['red', 'green', 'blue', 'yellow'], list(range(1,13)))
        self.discard_pile: DiscardPile = DiscardPile()

        # set up the players
        self.__create_players(players)

        # define starting player
        self.starting_player: Player = random.sample(self.players, 1)[0]

        # active player (when games start its the same as the starting player)
        self.active_player: Player = self.starting_player

        #open the first card from the deck, and add to the discard pile
        self.discard_pile.add_cards(self.card_deck.pick_card())


    # create players
    def __create_players(self, names: list) -> None:
        if not 2 <= len(names) <= 6:
            sys.exit("The amount of players allowed is between 2 and 6")
        
        for name in names:
            self.players.append(Player(name, self.card_deck.create_player_hand()))

    # set the next player as active or starting player
    def next_player(self) -> Player:
        idx = 0
        for index, player in enumerate(self.players):
            if player == self.active_player:
                idx = index
        idx = (idx + 1) % len(self.players)
        
        return self.players[idx]