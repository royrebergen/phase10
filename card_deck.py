import random
from deck import Deck

class CardDeck(Deck):
    def __init__(self, \
                    colors: list = None,\
                    numbers: list = None,\
                    jokers: int = 8,\
                    aussitzen: int = 4,\
                    shuffled: bool = True
                    ):

        self.colors = colors
        self.numbers = numbers
        self.jokers = jokers
        self.aussitzen = aussitzen
        self.shuffled = shuffled

        self.deck = []

        self.__build_card_deck([], self.colors, self.numbers)
        

     # card deck builder
    def __build_card_deck(self, deck: list, colors: list, numbers: list, jokers: int = 8, aussitzen: int = 4, shuffled: bool = True) -> None:
        deck+= [('joker', 'joker')]*jokers
        deck+= [('aussetzen', 'aussetzen')]*aussitzen
        for col in colors:
            # all colors have the numbers 1 to 12, 2 times
            for x in range(2):
                for number in numbers:
                    deck+= [(col, number)]
        if shuffled:
            random.shuffle(deck)
        self.deck = deck

    # Create player hand
    def create_player_hand(self) -> list:
        hand = random.sample(self.deck, 10)
        self.__remove_hand_from_card_deck(hand)
        return hand

    # Remove hands from card deck
    def __remove_hand_from_card_deck(self, hand: list) -> None:
        for x in hand:
            self.deck.remove(x)
