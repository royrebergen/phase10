class Player:
    def __init__(self, name: str, hand: list):
        self.name: str = name
        self.hand: list = hand
        self.on_table: bool = False
        self.aussetzen: bool = False
        self.penalty_points: int = 0
        self.phase: int = 1

    def __str__(self):
        return f'Player({self.name}, {self.hand}, on_table:{self.on_table}, aussetzen:{self.aussetzen}, penalty_points:{self.penalty_points}, phase:{self.phase})'

    def calculate_penalty_points(self) -> int:
        """Calculate the total amount of penalty points by the cards in the players hand
        The current hand will be added up by the players total penalty points collected in the game.

        - five points(5) for each card with value 1-9
        - ten points(10) for each card with value 10-12
        - fifteen points(15) for a Skip
        - twenty-five points(25) for a Wild
        source: https://en.wikipedia.org/wiki/Phase_10#Going_out_/_finishing_a_hand

        Return value:
        penalty_this_round -- Int with the sum of the penalty points in this round
        """
        penalty_this_round: int = 0
        for x in self.hand:
            y = x[1] # we use the 1 index of the card tuples eg (green, 10)
            if y == "aussetzen":
                penalty_this_round+= 15
            elif y == "joker":
                penalty_this_round+= 25
            elif y in range(1, 10):
                penalty_this_round+= 5
            elif y in range(10, 13):
                penalty_this_round+= 10
        self.penalty_points+= penalty_this_round
        return penalty_this_round

    def add_card_to_hand(self, card: list) -> None:
        self.hand.extend(card)

    def remove_card_from_hand(self, index: int) -> None:
        del self.hand[index]