from deck import Deck
from typing import Union

class DiscardPile(Deck):
    def __init__(self):
        self.deck = []

    def __str__(self) -> str:
        return f'DiscardPile({self.deck[-1]})'

    def show(self) -> Union[bool, str]:
        if len(self.deck) > 0:
            return True, f'DiscardPile({self.deck[-1]})'
        return False, f'No cards in the discard pile!'